import React from 'react';
import { configure, mount, ReactWrapper } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import sinon from 'sinon';
import Tempo from 'Tempo/Tempo';

interface ISpy {
  [s: string]: {
    [s: string]: sinon.SinonSpy
  }
}

configure({ adapter: new Adapter() });

let tempo: ReactWrapper;
let tempoInstance: Tempo;
let spy: ISpy = {};

describe('from / to', () => {
  beforeEach(() => {
    // Sinon
    spy = {
      Tempo: {
        getDist: sinon.spy(Tempo.prototype, "getDist"),
        changeTempo: sinon.spy(Tempo.prototype, "changeTempo"),
        mouseDown: sinon.spy(Tempo.prototype as any, "mouseDown"),
        mouseMove: sinon.spy(Tempo.prototype as any, "mouseMove"),
        mouseUp: sinon.spy(Tempo.prototype as any, "mouseUp"),        
      },
    };
    sinon.replace(Tempo.prototype as any, 'getWidth', () => 500);

    // Enzyme
    tempo = mount(
      <Tempo 
        role="from" 
        tempo={80} 
        maxDelta={100} 
        range={{
          from: 50,
          to: 200
        }} 
        isHandlerEnabled={true}
      />);
    tempoInstance = tempo.instance() as Tempo;
    
    // Sinon - reset
    sinon.resetHistory();
  });
  afterEach(() => {
    expect(tempo).toMatchSnapshot();
    tempo.unmount();
    sinon.restore();
  });
  it('changes tempo', () => {
    tempoInstance.changeTempo(150);
    expect(tempoInstance.tempo).toBe(150);
  });
});
